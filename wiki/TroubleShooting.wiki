*Please read below if you have any problems in getting the mod to work.*


Instillation Problems

I don’t know where to install the mod 

*For vista* copy the mod files to this directory 

_C:\Users\"your user name"\AppData?\Local\Ironclad Games\Sins of a Solar Empire\Mods-Entrenchment v1.03_

*For XP* copy the mod files to this directory 

_C:\Documents and Settings\yourusername\local settings\ Application Data\Ironclad Games\ Sins of a solar empire\Mods-Entrenchment v1.03_

*Team coloured ships and stations*

My ships look like my team colours (red green blue etc) ? 

You must start the game and go to the graphics options 

Here you will see two options 

_Ship Highlight mesh filter and
Ship Team colours_

*Disable* both of these

*I have installed the mod but the mod does not show up in the mod selection screen*

Please delete the *EnabledMods.txt* located in the Mods-Entrenchment v1.03 folder